#!/usr/bin/python

# Create Sites from CSV file.


# =====
# IMPORTS
# =====

import sys, requests
import csv, json
import time

from tabulate import tabulate
from netaddr import IPNetwork, IPAddress
from config import *


# =====
# VARIABLES
# =====

file = 'sites.csv'
claim_file = 'claimcodes.csv'
epoch_time = int(time.time())


# =====
# FUNCTIONS
# =====

# Convert CSV file to JSON
def csv_to_json(file):
	csv_rows = []
	
	with open(file, 'r') as csv_file:
		reader = csv.DictReader(csv_file)
		title = reader.fieldnames

		for row in reader:
			csv_rows.extend([{title[i]: row[title[i]] for i in range(len(title))}])

	return csv_rows

#Convert CSV file to List
def csv_to_list(file):
	with open(file, 'r') as csv_file:
		reader = csv.reader(csv_file)
		your_list = list(reader)

	return your_list

# Create Site
def create_site(session, site):
	url = '{}/orgs/{}/sites'.format(api_url, org_id)
	payload = json.dumps(site)

	result = session.post(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return None

	result = json.loads(result.text)
	return result['id']

# Claim APs
def update_inventory(session, setting):
	url = '{}/orgs/{}/inventory'.format(api_url, org_id)
	payload = json.dumps(setting)

	result = session.post(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return None

	result = json.loads(result.text)
	return result

# Configure Site
def update_site_setting(session, site_id, setting):
	url = '{}/sites/{}/setting'.format(api_url, site_id)
	payload = json.dumps(setting)

	result = session.put(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return False

	return True


# Get Site Group ID by Name (if it exists)
def get_sitegroup(session, name, result=[]):
	url = '{}/orgs/{}/sitegroups'.format(api_url, org_id)
	result = session.get(url, headers=headers)

	if result.status_code != 200:
		print('Failed to GET')
		print('URL: {}'.format(url))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return (False, None, result)

	result = json.loads(result.text)
	try:
		# Return first matching sitegroup
		return next((True, r['id'], result) for r in result if r['name'] == name)
	except:
		return (True, None, result)


# Get all Site Groups
def get_sitegroups(session, result=[]):
	if result == []:
		url = '{}/orgs/{}/sitegroups'.format(api_url, org_id)
		result = session.get(url, headers=headers)
	
		if result.status_code != 200:
			print('Failed to GET')
			print('URL: {}'.format(url))
			print('Response: {} ({})'.format(result.text, result.status_code))

			return []

	result = json.loads(result.text)
	return result

def get_sites(session):

	url = '{}/orgs/{}/sites'.format(api_url, org_id)
	result = session.get(url, headers=headers)

	if result.status_code != 200:
		print('Failed to GET')
		print('URL: {}'.format(url))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return []

	result = json.loads(result.text)
	return result

# Create Site Group
def create_sitegroup(session, name):
	url = '{}/orgs/{}/sitegroups'.format(api_url, org_id)

	payload = json.dumps({ 'name': name })

	result = session.post(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return None

	result = json.loads(result.text)
	return result['id']


# Subscribe to notification
def subscribe_to_notifications(session, site_id):
	url = '{}/sites/{}/subscriptions'.format(api_url, site_id)
	payload = None

	result = session.post(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return False

	return True

def get_geocode(address):
	address.replace(' ','+')
	url = 'https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}'.format(address, google_api)

	result = requests.post(url)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return None

	result = json.loads(result.text)
	return result

def get_timezone(location):
	url = 'https://maps.googleapis.com/maps/api/timezone/json?location={}&timestamp={}&key={}'.format(location, epoch_time, google_api)

	result = requests.post(url)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return None

	result = json.loads(result.text)
	return result

def get_device_stats(session):
	url = '{}/sites/{}/stats/devices'.format(api_url, staging_site_id)
	result = session.get(url, headers=headers)

	if result.status_code != 200:
		print('Failed to GET')
		print('URL: {}'.format(url))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return []

	result = json.loads(result.text)
	return result

def get_single_device_stats(session, ap_site_id, ap_id):
	url = '{}/sites/{}/stats/devices/{}'.format(api_url, ap_site_id, ap_id)
	result = session.get(url, headers=headers)

	if result.status_code != 200:
		print('Failed to GET')
		print('URL: {}'.format(url))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return []

	result = json.loads(result.text)
	return result

def move_device(session, ap_setting):
	url = '{}/orgs/{}/inventory'.format(api_url, org_id)
	payload = json.dumps(ap_setting)

	result = session.put(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return False

	return True

def update_device(session, ap_setting, ap_site_id, ap_id):
	url = '{}/sites/{}/devices/{}'.format(api_url, ap_site_id, ap_id)
	payload = json.dumps(ap_setting)

	result = session.put(url, headers=headers, data=payload)

	if result.status_code != 200:
		print('Failed to POST')
		print('URL: {}'.format(url))
		print('Payload: {}'.format(payload))
		print('Response: {} ({})'.format(result.text, result.status_code))

		return False

	return True

def add_sites():

	# Ensure variables are defined
	if api_token == '' or org_id == '' or file == '' or api_url == '':
		print('Missing variables:')
		print('api_token={}'.format(api_token))
		print('org_id={}'.format(org_id))
		print('file={}'.format(file))
		print('api_url={}'.format(api_url))

		sys.exit(1)

	# Create session
	session = requests.Session()

	# Convert sites.csv file to json
	csv_data = csv_to_json(file)

	existing_sitegroups = get_sitegroups(session)


	# Create site for each row in the file
	for row in csv_data:
		# Ensure CSV Row contains 'Site Name', else skip to the next row
		if row['Site Name'] == '':
			print('Missing: Site Name')

			continue


		# Create Site Group
		sitegroup_ids = []
		if 'Site Group' in row and row['Site Group'] != '':
			sitegroups = [x.strip() for x in row['Site Group'].split(',')]

			for sitegroup in sitegroups:
				if sitegroup == '':
					continue

				(found, sitegroup_id, existing_sitegroups) = get_sitegroup(session, sitegroup, existing_sitegroups)
				if found == False:
					print('Failed to get site group')

					continue

				if sitegroup_id != None:
					print('Found existing site group \'{}\' ({})'.format(sitegroup, sitegroup_id))

					sitegroup_ids.append(sitegroup_id)
				else:
					sitegroup_id = create_sitegroup(session, sitegroup)

					if sitegroup_id == None:
						print('Failed to create site group {}'.format(sitegroup))
					else:
						print('Created site group \'{}\' ({})'.format(sitegroup, sitegroup_id))

						sitegroup_ids.append(sitegroup_id)

					existing_sitegroups = get_sitegroups(session)


		# Create Site
		## Construct the site object
		## Uses Google's Geocoding API to determine the address, timezone, country_code, and latlng
		## https://developers.google.com/maps/documentation/geocoding/start
		location = get_geocode(row['Address'])
		lat = location['results'][0]['geometry']['location']['lat']
		lng = location['results'][0]['geometry']['location']['lng']
		timezone = get_timezone('{}, {}'.format(lat, lng))['timeZoneId']
		site = {
			'country_code': row['Country'],
			'timezone': timezone,
			'address': row['Address'],
			'latlng': {  
				'lat': lat,
				'lng': lng
			},
			'name': row['Site Name'].strip(),
			'sitegroup_ids': list(set(sitegroup_ids))
		}

		site_id = create_site(session, site)
		if site_id == None:
			print('Failed to create site {}'.format(site['name']))

			continue
		else:
			print('Created site \'{}\' ({})'.format(site['name'], site_id))


		# Update Site Setting
		## Construct the site setting object
		site_setting = {
			'rtsa': {
				'enabled': True
			},
			'auto_upgrade': {
				'enabled': False
			},
			'rogue': {
				'min_rssi': -80,
				'enabled': True,
				'honeypot_enabled': True
			}
		}
		result = update_site_setting(session, site_id, site_setting)
		if result:
			print('Updated site setting {} ({})'.format(site['name'], site_id))
		else:
			print('Failed to update site setting {} ({})'.format(site['name'], site_id))


		# Subscribe to Site Notifications
		if 'Subscribe' in row and row['Subscribe'] != '':
			enable = False

			try:
				enable = eval(row['Subscribe'].strip().title())
			except Exception:
				print('Invalid Subscribe value: {}'.format(row['Subscribe']))

			if enable:
				result = subscribe_to_notifications(session, site_id)
				if result:
					print('Subscribed to site notifications ({})'.format(site_id))
				else:
					print('Failed to subscribe to site notifications ({})'.format(site_id))

		print()
	return

def claim_aps():
	if api_token == '' or staging_site_id == '' or api_url == '':
		print('Missing variables:')
		print('api_token={}'.format(api_token))
		print('staging_site_id={}'.format(staging_site_id))
		print('api_url={}'.format(api_url))

		sys.exit(1)

	# Create session
	session = requests.Session()

	# Convert sites.csv file to json
	csv_data = csv_to_list(claim_file)
	

	result = update_inventory(session, csv_data[0])

	mac_list = []

	for device in result['inventory_added']:
		#print(device)
		mac_list.append(device['mac'])

	print(mac_list)
	ap_setting = {
			"op": "assign",
			"site_id": staging_site_id,
			"macs": mac_list,
			"no_reassign": False
		}

	result = move_device(session, ap_setting)
	
	if result:
		print('Updated AP {} with site Staging ({})'.format(device['mac'], staging_site_id))
	else:
		print('Failed to update AP {} with site Staging ({})'.format(device['mac'], staging_site_id))

def move_aps():
	if api_token == '' or staging_site_id == '' or api_url == '':
		print('Missing variables:')
		print('api_token={}'.format(api_token))
		print('staging_site_id={}'.format(staging_site_id))
		print('api_url={}'.format(api_url))

		sys.exit(1)

	# Create session
	session = requests.Session()

	# Convert sites.csv file to json
	csv_data = csv_to_json(file)

	# Get Site IDs and names
	sites = get_sites(session)
	site_name_id = {}
	for site in sites:
		site_name_id[site['name']] = site['id']

	site_name_subnet = {}
	for row in csv_data:
		# Ensure CSV Row contains 'Site Name', else skip to the next row
		if row['Site Name'] == '':
			print('Missing: Site Name')

			continue

		# Ensure CSV Row contains 'Site Name', else skip to the next row
		if row['Subnet'] == '':
			print('Missing: Subnet')

			continue

		site_name_subnet[row['Site Name']] = row['Subnet']
		#print(site_name_subnet)

	# Get AP stats
	devices = get_device_stats(session)
	res = []
	for device in devices:
		for name, subnet in site_name_subnet.items():
			if IPAddress(device['ip_stat']['ip']) in IPNetwork(subnet):
				ap_site_name = name
				print("AP IP address {} matches subnet {} at {}".format(device['ip_stat']['ip'], subnet, ap_site_name))

		ap_site_id = site_name_id[ap_site_name]
		#print(ap_site_id)

		ap_name = '{} AP {} {}'.format(ap_site_name, device['lldp_stat']['system_name'], device['lldp_stat']['port_desc'])
		ap_id = device['id']

		ap_setting = {
			"op": "assign",
			"site_id": ap_site_id,
			"macs": [
				device['mac']
			],
			"no_reassign": False
		}

		result = move_device(session, ap_setting)

		if result:
			print('Updated AP {} with site {} ({})'.format(device['mac'], ap_site_name, ap_site_id))
		else:
			print('Failed to update AP {} with site {} ({})'.format(device['mac'], ap_site_name, ap_site_id))

		ap_setting = {
			"name": ap_name,
		}

		result = update_device(session, ap_setting, ap_site_id, ap_id)

		if result:
			print('Updated AP {} with name {}'.format(device['mac'], ap_name))
		else:
			print('Failed to update AP {} with name {}'.format(device['mac'], ap_name))

		res.append(
			[
				device['mac'], 
				ap_name, 
				device['model'],
				'{}({})'.format(ap_site_name, ap_site_id),
				subnet,
				device['ip_stat']['ip'] if 'ip_stat' in device and 'ip' in device['ip_stat'] else '',
				device['ip_stat']['gateway'] if 'ip_stat' in device and 'gateway' in device['ip_stat'] else '',
				device['lldp_stat']['system_name'] if 'lldp_stat' in device and 'system_name' in device['lldp_stat'] else '',
				device['lldp_stat']['port_desc'] if 'lldp_stat' in device and 'port_desc' in device['lldp_stat'] else ''
			]
		)

	print(tabulate(res, headers=['MAC', 'Name', 'Model', 'Site', 'Site Subnet', 'IP Address', 'Gateway', 'LLDP Neighbour', 'LLDP Neighbour Port'], tablefmt='fancy_grid'))

# Main
if __name__ == '__main__':
	ans=True
	while ans:
		print ("""
		1.Add Sites
		2.Claim APs and move to Staging
		3.Add APs to Sites and Rename
		4.Exit/Quit
		""")

		ans=input("What would you like to do? ") 
		if ans=="1": 
			print("\n Adding Sites")
			add_sites()
		elif ans=="2":
			print("\n Claiming APs and move to Staging") 
			claim_aps()
		elif ans=="3":
			print("\n Adding APs to Sites") 
			move_aps()
		elif ans=="4":
			print("\n Goodbye")
			sys.exit(0)
		elif ans !="":
			print("\n Not Valid Choice Try again") 

